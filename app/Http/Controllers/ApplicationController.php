<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Mail\ApplicationMail;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller
{
    public function send(ApplicationRequest $request){
        $lang = App::getLocale();

        //telegram
        $token =  env('TELEGRAM_TOKEN');
        $chat_id = env('TELEGRAM_CHAT_ID');
        $txt="";
        $arr = array(
            'Компания' => $request->post('company'),
            'Контактное лицо' => $request->post('contact-person'),
            'Телефон ' => $request->post('phone'),
        );
        $txt .= "<b>Заявка</b>%0A";
        foreach($arr as $key => $value) {
            $txt .= "<b>".$key."</b>: ".$value."%0A";
        }
        /*try {
            $sendToTelegram = fopen("https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chat_id}&parse_mode=html&text={$txt}","r");
        }catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
        }*/

        //email
        //Mail::to(env('TO_EMAIL'))->send(new ApplicationMail($request));

        $response = [];
        switch ($lang) {
            case 'ru':
                $response['success_message'] = "Заявка успешно отправлена!";
                break;
            case 'en':
                $response['success_message'] = "Application successfully sent!";
                break;
            case 'kk':
                $response['success_message'] = "Өтінім сәтті жіберілді!";
                break;
            default:
                $response['success_message'] = "Заявка успешно отправлена!";
        }
        return $response;
    }
}
