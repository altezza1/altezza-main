<!DOCTYPE html>
@php $lang = App::getLocale() @endphp
<html lang="{{ $lang }}">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="stylesheet" href="/css/locomotive-scroll.css">
    <link rel="stylesheet" href="/fonts/stylesheet.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" type="image/x-icon" href="/img/logo.svg">
    <title>Alliance Telecom</title>
</head>

<body>
    <header>
        <div class="container">
            <a href="#top" class="anchor"><img class="logo img-svg" src="/img/logo.svg" alt=""></a>
            <div class="menu">
                <div class="menu-toggle __top">
                    <span>Menu</span>
                    <div class="circle">
                        <svg width="10" height="6" viewBox="0 0 10 6" fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M6.78375 1.18896L9.35132 3.24302M9.35132 3.24302L6.78375 5.29707M9.35132 3.24302L1.1351 3.24302"
                                stroke="white" stroke-width="0.633333" stroke-linecap="round" stroke-linejoin="round" />
                        </svg>
                    </div>
                </div>
                <div class="__inner">
                    <nav>
                        <ul>
                            <li><a href="#about-us">About Us</a></li>
                            <li><a href="#our-values">Our values</a></li>
                            <li><a href="#our-resources">Our resources</a></li>
                            <li><a href="#esg">ESG</a></li>
                            <li><a href="#invest-strategy">Investment strategy</a></li>
                            <li><a href="#projects">Projects</a></li>
                            {{-- <li><a href="#structure">Structure</a></li> --}}
                            <li><a href="#footer">Contacts</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <div id="content" class="smooth-scroll" data-scroll-container>
        @yield('content')
        <footer id="footer">
            <div class="container">
                <div class="row jc-space-between">
                    <div class="logo-col"><a href="/"><img class="logo" src="/img/logo.svg" alt=""></a>
                    </div>
                    <div>
                        <h5>Our companies</h5>
                        <nav>
                            <ul>
                                <li><a href="https://silvertelecom.kz/" target="_blank">Silver Telecom</a></li>
                                <li><a href="https://vista-technology.kz" target="_blank">Vista</a></li>
                                <li><a href="https://ipnet.kg" target="_blank">Ipnet</a></li>
                                <li><a href="https://europeer.de" target="_blank">Europeer Xchange</a></li>
                                <li><a href="https://dc.kg/ru/glavnaya/" target="_blank">Дата Центр</a></li>
                                <li><a href="https://fiberlinks.kg/" target="_blank">Файбер Линкс</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div>
                        <h5>Menu</h5>
                        <nav>
                            <ul>
                                <li><a class="anchor" href="#about-us">About Us</a></li>
                                <li><a class="anchor" href="#our-values">Our values</a></li>
                                <li><a class="anchor" href="#our-resources">Our resources</a></li>
                                <li><a class="anchor" href="#esg">ESG</a></li>
                                <li><a class="anchor" href="#invest-strategy">Investment strategy</a></li>
                                <li><a class="anchor" href="#projects">Projects</a></li>
                                {{-- <li><a class="anchor " href="#structure">Structure</a></li> --}}
                            </ul>
                        </nav>
                    </div>
                    <div>
                        <h5>Contacts</h5>
                        <div class="contact-items">
                            <div>
                                {{-- <span class="group-name">Email:</span> --}}
                                <a href="mailto: info@alliance-telecom.com">info@alliance-telecom.com</a>
                                <p style="margin-top: 10px">Rivierdijk 352,Hardinxveld-Giessendam,3372 BS Netherlands
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="__bottom">
                    <span>All rights reserved © Alliance Telecom</span>
                    <span>Powered by <a href="https://brainteam.kz/ru" target="_blank">BrainTeam</a> ©
                        {{ date('Y') }}</span>
                </div>
            </div>
        </footer>
    </div>
    <script src="/js/CustomEase.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollTrigger.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/ScrollToPlugin.min.js"></script>
    <script src="/js/locomotive-scroll.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/js/script.js"></script>
</body>

</html>
