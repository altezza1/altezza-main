@extends('layouts.master')
@section('content')
    <section id="top">
        <img class="top-bg" src="/img/top-bg.jpg" alt="" data-scroll data-scroll-speed="-2">
        <div class="container">
            <div class="row">
                <div>
                    <h1>
                        Your dedicated partner in the telecommunications & IT industry
                    </h1>
                    <p>Connecting worlds, releasing opportunities</p>
                    <div class="big-circle">
                        <svg width="13" height="23" viewBox="0 0 13 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.7998 15.2221L6.64425 21.6665M6.64425 21.6665L1.48869 15.2221M6.64425 21.6665L6.64425 1.04428" stroke="white" stroke-width="1.28889" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </div>
                </div>
                {{--<div class="big-a-col">
                    <img src="/img/big-A.svg" alt="">
                    <img class="animate-A" src="/img/big-A.svg" alt="">
                    <img class="animate-A" src="/img/big-A.svg" alt="">
                    <img class="animate-A" src="/img/big-A.svg" alt="">
                </div>--}}
            </div>
        </div>
    </section>
    <section id="about-us" class="on-white-zone">
        <div class="container">
            <div class="row jc-space-between">
                <div class="text-col">
                    <h2 class="section-title animation_fade-top" data-scroll>About Us</h2>
                    <h3>Alliance Telecom - is</h3>
                    <div class="info-item">
                        A telecommunications holding company with a suite of highly qualified projects and a scalable presence on the map (Europe, Central Asia, Southeast Asia)
                    </div>
                    <div class="info-item">
                        Flexible partnership conditions
                    </div>
                    <div class="info-item">
                        Analytical filtering of candidates for providing an opportunity to participate in the project <b>Alliance Telecom</b>
                    </div>
                    <div class="info-item">
                        A team of professional investors and experts in the telecommunications and IT industry as a key to high productivity and profitability of the investments
                    </div>
                    <h3 class="holding-mission">Holding's mission</h3>
                    <p>
                        Maintaining a balance in interactions and mutual benefits, to create the biggest communications operator in networks coverage for delivering highly qualified telecoms and IT services via its own and partners' networks. Binding communications operators with each other from around the world, to benefit the society by participating in the scaling up of the Internet penetration in all countries of the world. As a result, we help people get access to the Internet possibilities as well as to the development of IT services.
                    </p>
                </div>
                <div>
                    <div class="img-overflow" data-scroll data-scroll-speed="4">
                        <img src="/img/about-us.jpg" alt="" data-scroll data-scroll-speed="-1.5" data-scroll-target="#about-us">
                    </div>
                </div>
                <div class="quotes-outer">
                    <img src="/img/quotes.svg" alt="">
                    <p>Possessing the extensive connectivity of telecommunications networks with the best itineraries, and a team of experts, to create new beneficial partnerships</p>
                </div>
            </div>
        </div>
    </section>
    <section id="our-values">
        <div class="container">
            <h2 class="section-title animation_fade-top" data-scroll>Our values</h2>
            <div class="row __top">
                <div class="animation_fade-top" data-scroll>
                    <h4>Innovations</h4>
                    <p>We constantly strive for new ideas and solutions to satisfy our clients' needs.</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <h4>Our global presence</h4>
                    <p>We work around the world providing partners with contemporary solutions and support required for the successful business development.</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <h4>Collaboration</h4>
                    <p>We believe in collaboration and partnership as means for achieving mutual benefit and growth.</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <h4>Reliability</h4>
                    <p>We are proud of the high quality of our goods and services, ensuring the stability and accessibility for our partners.</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <h4>Together into the future</h4>
                    <p>We take great pride in our technologies and communications solutions helping our clients create the future.</p>
                </div>
            </div>
        </div>
        <div class="container" id="our-resources">
            <img class="back-wsh" src="/img/back-wsh.svg" alt="">
            <h2 class="section-title animation_fade-top" data-scroll>Our resources</h2>
            <div class="big-txts">
                <div class="animation_fade-top" data-scroll>
                    <span>ROI</span>
                    <p>projects with high ROI in Asia, Europe and PRC</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <span>Expertise</span>
                    <p>indisputable professional expertise of our employees</p>
                </div>
                <div class="animation_fade-top" data-scroll>
                    <span>Partnership</span>
                    <p>partnership with regional operators for better connectivity, IT solutions in Covtech, e-commerce, fintech, and other directions</p>
                </div>
            </div>
        </div>
        <div class="container" id="esg">
            <div class="row jc-space-between">
                <div class="text-col">
                    <h2 class="section-title animation_fade-top" data-scroll>ESG</h2>
                    <h4>
                        Build up the future taking into account the planet, people and management
                    </h4>
                    <p class="info-item-2">
                        We support United Nations objectives of sustainable development that serve as a global call to action concerning environmental protection, elimination of inequality and improving the quality of people's life all around the world. In our work we are guided by the principles underlying these objectives to create a sustainable future for everyone.
                    </p>
                    <p class="info-item-2">
                        We accept serious challenges regarding water resources and we are ready to act. Our work in the sustainable development is focused on the preservation of clean water and its universal accessibility.
                    </p>
                    <p class="info-item-2">
                        We cooperate with partners to reduce water consumption and implement advanced water purification technologies. Our goal lies in providing the sustainable usage of water resources that not only helps ecosystems, but also improves the quality of life of our communities.
                    </p>
                    <p class="info-item-2">
                        Join us in an effort to make the world more sustainable, taking care of our planet and all inhabitants' future. Together we can make a difference in preserving our water resources.
                    </p>
                </div>
                <div class="img-overflow" data-scroll data-scroll-speed="4">
                    <img src="/img/esg.jpg" alt="" data-scroll data-scroll-speed="-1.5" data-scroll-target="#esg">
                </div>
            </div>
        </div>
    </section>
    <section id="invest-strategy" class="on-white-zone">
        <div class="container">
            <h2 class="section-title animation_fade-top" data-scroll>Investment strategy</h2>
            <p>We play a key role in enhancing the efficiency and unleashing the growth potential of the companies. Throughout well-defined leadership strategy and leverage of the best practices, we assist companies in their quick and effective evolution.</p>
            <div class="row info-items jc-space-between">
                <div class="info-item-2">
                    <h4>Investment geography</h4>
                    <p>Europe and Asia are investment priorities. Though, the company evinces willingness to consider appealing investment projects beyond the defined region.</p>
                </div>
                <div class="info-item-2">
                    <h4>Growth potential</h4>
                    <p>Investment projects are required to imply the growth potential which will  allow the company to increase their value, at least to double it within 5 years.</p>
                </div>
                <div class="info-item-2">
                    <h4>Branches</h4>
                    <p>A group of companies has comprehensive expertise and experience in the telecommunications and IT industry.</p>
                </div>
                <div class="info-item-2">
                    <h4>Industry leadership</h4>
                    <p>We are considering investments which possess the perspective to take a leading position in the industry and certain competitive advantages.</p>
                </div>
                <div class="info-item-2">
                    <h4>Professional team</h4>
                    <p>A strong managerial team with considerable experience and previous achievements is the crucial factor in making investment decisions.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="projects">
        <div class="container">
            <div class="row jc-space-between">
                <div>
                    <h2 class="section-title animation_fade-top white" data-scroll>Companies & projects</h2>
                    <h4>Collaboration as the first step to success</h4>
                    <p>We work in various directions, but the only unchangeable thing is the high professionalism of our team.</p>
                    <div class="partner-items">
                        <a class="animation_fade-top" target="_blank" href="https://silvertelecom.kz/" data-scroll>
                            <img src="/img/partner-1.svg" alt="">
                        </a>
                        <a class="animation_fade-top" target="_blank" href="https://vista-technology.kz" data-scroll>
                            <img src="/img/partner-2.svg" alt="">
                        </a>
                        <a class="animation_fade-top" target="_blank" href="https://ipnet.kg" data-scroll>
                            <img src="/img/partner-3.svg" alt="">
                        </a>
                        {{-- <a class="animation_fade-top" target="_blank" data-scroll>
                            <img src="/img/partner-4.svg" alt="">
                        </a> --}}
                        {{-- <a class="animation_fade-top" target="_blank" href="https://spay.kz" data-scroll>
                            <img src="/img/partner-5.svg" alt="">
                        </a> --}}
                        <a class="animation_fade-top" target="_blank" href="https://europeer.de" data-scroll>
                            <img src="/img/partner-6.svg" alt="">
                        </a>
                        
                        <a class="animation_fade-top" target="_blank" href="https://dc.kg/ru/glavnaya/" data-scroll>
                            <img src="/img/partner-7.png" alt="" style="width: 140px">
                        </a>
                        <a class="animation_fade-top" target="_blank" href="https://fiberlinks.kg/" data-scroll>
                            <img src="/img/partner-8.svg" alt="" style="width: 152px">
                        </a>
                    </div>
                </div>
                <div class="img-overflow" data-scroll data-scroll-speed="4">
                    <img src="/img/projects.jpg" alt="" data-scroll data-scroll-speed="-1.5" data-scroll-target="#projects">
                </div>
            </div>
        </div>
    </section>
    {{--<section id="structure">
        <div class="container">
            <h2 class="section-title animation_fade-top white" data-scroll>Structure</h2>
            <div class="row">
                <div>
                    <h3>Board of <br>directors</h3>
                </div>
                <div class="holding-col">
                    <h3>Holding</h3>
                    <div class="country-items">
                        <div class="animation_fade-top" data-scroll>
                            <img src="/img/country-1.png" alt="">
                            <span>Russia</span>
                        </div>
                        <div class="animation_fade-top" data-scroll>
                            <img src="/img/country-2.png" alt="">
                            <span>Singapore</span>
                        </div>
                        <div class="animation_fade-top" data-scroll>
                            <img src="/img/country-3.png" alt="">
                            <span>Kyrgyzstan</span>
                        </div>
                        <div class="animation_fade-top" data-scroll>
                            <img src="/img/country-4.png" alt="">
                            <span>Kazakhstan</span>
                        </div>
                        <div class="animation_fade-top" data-scroll>
                            <img src="/img/country-5.png" alt="">
                            <span>Germany</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>--}}
    <section id="map" style="margin-top: 40px">
        <div class="container">
            <h2 class="section-title animation_fade-top white" data-scroll>Map of networks coverage</h2>
        </div>
        <div class="map-outer">
            <img class=" animation_fade-top" src="/img/map.svg" alt="" data-scroll>
        </div>
    </section>
@endsection


