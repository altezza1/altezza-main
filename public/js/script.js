let body = $('body');
let mobile = false;

if ($(window).width() < 992){
    mobile = true;
}

/*Locomotive with Scrolltrigger*/
gsap.registerPlugin(ScrollTrigger, ScrollToPlugin);

// Using Locomotive Scroll from Locomotive https://github.com/locomotivemtl/locomotive-scroll
const locoScroll = new LocomotiveScroll({
    el: document.querySelector(".smooth-scroll"),
    smooth: true
});
// each time Locomotive Scroll updates, tell ScrollTrigger to update too (sync positioning)
locoScroll.on("scroll", ScrollTrigger.update);

// tell ScrollTrigger to use these proxy methods for the ".smooth-scroll" element since Locomotive Scroll is hijacking things
ScrollTrigger.scrollerProxy(".smooth-scroll", {
    scrollTop(value) {
        return arguments.length ? locoScroll.scrollTo(value, 0, 0) : locoScroll.scroll.instance.scroll.y;
    }, // we don't have to define a scrollLeft because we're only scrolling vertically.
    getBoundingClientRect() {
        return {top: 0, left: 0, width: window.innerWidth, height: window.innerHeight};
    },
    // LocomotiveScroll handles things completely differently on mobile devices - it doesn't even transform the container at all! So to get the correct behavior and avoid jitters, we should pin things with position: fixed on mobile. We sense it by checking to see if there's a transform applied to the container (the LocomotiveScroll-controlled element).
    pinType: document.querySelector(".smooth-scroll").style.transform ? "transform" : "fixed"
});

ScrollTrigger.addEventListener("refresh", () => locoScroll.update());
ScrollTrigger.refresh();

/*GSAP ANIMATION*/
const animatedElements = document.querySelectorAll('.animate-A');

let valueOffset = 0
let step = !mobile ? 50 : 25

animatedElements.forEach((element) => {
    valueOffset -= step
    console.log(valueOffset)
    gsap.to(element, {
        x: valueOffset,
        scrollTrigger: {
            scroller: (!mobile ? '.smooth-scroll' : ''),
            trigger: "section#top",
            start: "top top",
            end: 'bottom top',
            scrub: true,
        },
    });
});

gsap.to('.back-wsh', {
    rotateY: 90,
    scrollTrigger: {
        scroller: (!mobile ? '.smooth-scroll' : ''),
        trigger: "#our-resources",
        start: "top bottom",
        end: 'bottom top',
        scrub: true,
    },
});

ScrollTrigger.batch('.on-white-zone', {
    scroller: (!mobile ? '.smooth-scroll' : ''),
    start: "top-=100 top",
    onEnter: () => {
        $('header').addClass('white-zone')
    },
    onEnterBack: () => {
        $('header').addClass('white-zone')
    },
    onLeave: () => {
        $('header').removeClass('white-zone')
    },
    onLeaveBack: () => {
        $('header').removeClass('white-zone')
    },
});


$(function(){
    $(".input-phone").mask("+9(999) 999-99-99");
});

$("img.img-svg").each(function () {
    var $img = $(this);
    var imgClass = $img.attr("class");
    var imgURL = $img.attr("src");
    $.get(imgURL, function (data) {
        var $svg = $(data).find("svg");
        if (typeof imgClass !== "undefined") {
            $svg = $svg.attr("class", imgClass + " replaced-svg");
        }
        $svg = $svg.removeAttr("xmlns:a");
        if (!$svg.attr("viewBox") && $svg.attr("height") && $svg.attr("width")) {
            $svg.attr("viewBox", "0 0 " + $svg.attr("height") + " " + $svg.attr("width"))
        }
        $img.replaceWith($svg);
    }, "xml");
});

/*Ajax forms*/
$("form.__default").submit(function (e) {
    e.preventDefault();
    $('.btn[type="submit"]').prop('disabled', true);
    $.ajax({
        url: $(this).attr('action'),
        type: "POST",
        data: $(this).serialize(),
        success: function(response) {
            $('.btn[type="submit"]').prop('disabled', false);
            alert('Форма успешно отправлена!');
            $('.overlay-forms').fadeOut();
        },
        error: function(response) {
            $('.btn[type="submit"]').prop('disabled', false);
            console.log(response);
            alert('Ошибка!');
        }
    });
});

/*menu*/
$('.anchor, .menu nav a').click(function (e) {
    e.preventDefault()
    let el = $(this).attr('href')
    locoScroll.scrollTo(el)
})

$('.menu-toggle').click(function () {
    $('.menu').toggleClass('active')
})

$('.menu nav a').click(function () {
    $('.menu').removeClass('active')
})

jQuery(function($){
    $(document).mouseup( function(e){ // событие клика по веб-документу
        var div = $( ".menu" ); // тут указываем ID элемента
        if ( !div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0 ) { // и не по его дочерним элементам
            $('.menu').removeClass('active')
        }
    });
});

/*var swiper_partners = new Swiper("#swiper_partners", {
    slidesPerView: "4",
    spaceBetween: 70,
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        991: {
            slidesPerView: 2,
            spaceBetween: 60,
        }
    }
});*/

$('.big-circle').click(function () {
    locoScroll.scrollTo('#about-us')
})

/*FULL LOAD*/
window.onload = function() {
    locoScroll.update();
    ScrollTrigger.refresh();
    $('html').addClass('loaded');
};
